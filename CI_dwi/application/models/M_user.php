<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_user extends CI_Model
{
    public function getAll($_table)
    {
        return $this->db->get($_table);
    }

    public function insert($_table, $data)
    {
        $this->db->insert($_table, $data);
    }

    public function update_data($data, $_table)
    {
        $this->db->where('id', $data['id']);
        $this->db->update($_table, $data);
    }

    public function update_buku($data_buku, $_table)
    {
        $this->db->where('id_buku', $data_buku['id_buku']);
        $this->db->update($_table, $data_buku);
    }

    public function update_kategori($data_kategori, $_table)
    {
        $this->db->where('id_kategori', $data_kategori['id_kategori']);
        $this->db->update($_table, $data_kategori);
    }

    public function delete($where, $_table)
    {
        $this->db->where($where);
        $this->db->delete($_table);
    }

    // public function data_buku($buku)
    // {
    //     return $this->db->get($buku);
    // }

    public function jumlah_user()
    {
        $this->db->select('*');
        $this->db->from('user');
        return $this->db->get()->num_rows();
    }

    public function jumlah_buku()
    {
        $this->db->select('*');
        $this->db->from('tb_buku');
        return $this->db->get()->num_rows();
    }

    public function jumlah_kategori()
    {
        $this->db->select('*');
        $this->db->from('tb_kategori');
        return $this->db->get()->num_rows();
    }

    public function join($_table, $tbljoin, $join)
    {
        $this->db->join($tbljoin, $join);
        return $this->db->get($_table);
    }

    public function getKategori()
    {
        $kategori = $this->db->query("SELECT * FROM tb_kategori ORDER BY nama_kategori");
        return $kategori->result();
    }

    public function getTbBuku()
    {
        $tbBuku = $this->db->query("SELECT * FROM tb_buku tb JOIN tb_kategori tk ON tb.id_kategori = tk.id_kategori ORDER BY tb.id_kategori");

        return $tbBuku->result();
    }

    public function simpan($data)
    {
        $this->db->insert('tb_buku', $data);

        return true;
    }

    public function get_keyword($keyword)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->like('username', $keyword);
        $this->db->or_like('name', $keyword);
        return $this->db->get()->result();
    }

    public function get_keywordKategori($keyword)
    {
        $this->db->select('*');
        $this->db->from('tb_kategori');
        $this->db->like('nama_kategori', $keyword);
        return $this->db->get()->result();
    }

    public function get_keywordBuku($keyword)
    {
        $this->db->select('*');
        $this->db->from('tb_buku');
        $this->db->like('judul_buku', $keyword);
        $this->db->or_like('pengarang', $keyword);
        $this->db->or_like('penerbit', $keyword);
        $this->db->or_like('tahun', $keyword);
        $this->db->or_like('id_kategori', $keyword);
        return $this->db->get()->result();
    }

}
