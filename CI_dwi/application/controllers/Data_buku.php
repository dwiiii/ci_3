<?php

class Data_buku extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_user');
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));
        //     if (!$this->session->userdata('login')) {
        //         redirect(base_url("auth"));
        //     }
    }

    public function index()
    {

        $data['buku'] = $this->m_user->getTbBuku();
        $data['dataKategori'] = $this->m_user->getKategori();
        $this->load->view('latihan/table_buku', $data);
    }

    public function tambah_buku()
    {
        $judulBuku = $this->input->post('judul_buku');
        $pengarang = $this->input->post('pengarang');
        $penerbit = $this->input->post('penerbit');
        $tahun = $this->input->post('tahun');
        $idKategori = $this->input->post('id_kategori');

        $data = array(
            'judul_buku' => $judulBuku,
            'pengarang' => $pengarang,
            'penerbit' => $penerbit,
            'tahun' => $tahun,
            'id_kategori' => $idKategori
        );

        $simpan = $this->m_user->simpan($data);

        if($simpan) {
            $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Buku berhasil ditambahkan
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>');
            redirect('data_buku/index', 'refresh');
        }else{
            $this->load->view('latihan/table_buku', 'refresh');
        }
    }

//     public function tambah_buku()
//     {
//         $this->rules();

//         if ($this->form_validation->run() == false) {
//             $this->load->view('latihan/table_buku');
//         } else {
//             $data = [
//                 'judul_buku' => htmlspecialchars($this->input->post('judul_buku', true)),
//                 'pengarang' => htmlspecialchars($this->input->post('pengarang', true)),
//                 'penerbit' => htmlspecialchars($this->input->post('penerbit', true)),
//                 'tahun' => htmlspecialchars($this->input->post('tahun'), true),
//                 'id_kategori' => $this->input->post('id_kategori', true)
//             ];

//             $this->db->insert('tb_buku', $data);
//             $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
//     Buku berhasil ditambahkan
//     <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
// </div>');
//             redirect('data_buku/index');
//         }
//     }

    public function edit($id)
    {
        $data_buku = [
            'id_buku' => $id,
            'judul_buku' => htmlspecialchars($this->input->post('judul_buku', true)),
            'pengarang' => htmlspecialchars($this->input->post('pengarang', true)),
            'penerbit' => htmlspecialchars($this->input->post('penerbit', true)),
            'tahun' => htmlspecialchars($this->input->post('tahun', true)),
            'id_kategori' => $this->input->post('id_kategori', true),
        ];
        $this->m_user->update_buku($data_buku, 'tb_buku');
        $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
        Buku berhasil di edit
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>');
        redirect('data_buku/index');
    }

    public function delete($id)

    {
        $where = array('id_buku' => $id);

        $this->m_user->delete($where, 'tb_buku');
        $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
            BUku berhasil dihapus
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>');
        redirect('data_buku/index');
    }

    public function rules()
    {
        $this->form_validation->set_rules('judul_buku', 'Judul', 'required|trim');
        $this->form_validation->set_rules('pengarang', 'Pengarang', 'required|trim');
        $this->form_validation->set_rules('penerbit', 'Penerbit', 'required|trim');
        $this->form_validation->set_rules('tahun', 'Tahun Terbit', 'required|trim');
    }

    public function search()
    {
        $keyword = $this->input->post('keyword');
        $data['buku'] = $this->m_user->get_keywordBuku($keyword);
        $data['dataKategori'] = $this->m_user->getKategori();
        $this->load->view('latihan/table_buku', $data);
    }
}
