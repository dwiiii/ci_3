<?php

class Data_user extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_user');
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));
    }

    public function index()
    {

        $data['user'] = $this->m_user->getAll('user')->result();

        $this->load->view('latihan/user', $data);
    }

    public function tambah_aksi()
    {
        $this->rules();

        if ($this->form_validation->run() == false) {
            $this->load->view('data_user/tambah_aksi');
        } else {
            $data = [
                'username' => htmlspecialchars($this->input->post('username', true)),
                'name' => htmlspecialchars($this->input->post('name', true)),
                'email' => htmlspecialchars($this->input->post('email', true)),
                'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'level' => $this->input->post('level', true)
            ];

            $this->db->insert('user', $data);
            $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    User berhasil ditambahkan
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>');
            redirect('data_user/index');
        }
    }

    public function edit($id)
    {
        $data = [
            'id' => $id,
            'username' => htmlspecialchars($this->input->post('username', true)),
            'name' => htmlspecialchars($this->input->post('name', true)),
            'email' => htmlspecialchars($this->input->post('email', true)),
            'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
            'level' => $this->input->post('level', true)
        ];
        $this->m_user->update_data($data, 'user');
        $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
        Data berhasil di edit
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>');
        redirect('data_user/index');
    }



    public function rules()
    {
        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
            'is_unique' => 'This email has already registered!'
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[6]|matches[password2]', [
            'matches' => 'Password dont match!',
            'min_length' => 'Password too short!'
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');
        $this->form_validation->set_rules('foto', 'Foto');
    }

    public function delete($id)
    {
        $where = array('id' => $id);

        $this->m_user->delete($where, 'user');
        $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
            Data berhasil dihapus
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>');
        redirect('data_user/index');
    }

    public function search()
    {
        $keyword = $this->input->post('keyword');
        $data['user'] = $this->m_user->get_keyword($keyword);
        $this->load->view('latihan/user', $data);
    }
}
