<?php

class Kategori extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_user');
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));
        //     if (!$this->session->userdata('login')) {
        //         redirect(base_url("auth"));
        //     }
    }

    public function index()
    {

        $data['tb_kategori'] = $this->m_user->getAll('tb_kategori')->result();
        $this->load->view('latihan/table_kategori', $data);
    }

    public function tambah()
    {
        $this->rules();

        if ($this->form_validation->run() == false) {
            $this->load->view('latihan/table_kategori');
        } else {
            $data = [
                'nama_kategori' => htmlspecialchars($this->input->post('nama_kategori', true))
            ];

            $this->db->insert('tb_kategori', $data);
            $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
        Buku berhasil ditambahkan
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>');
            redirect('kategori/index');
        }
    }

    public function rules()
    {
        $this->form_validation->set_rules('nama_kategori', 'Nama Kategori', 'required|trim');
    }

    public function edit($id_kategori)
    {
        $data_kategori = [
            'id_kategori' => $id_kategori,
            'nama_kategori' => htmlspecialchars($this->input->post('nama_kategori', true))
        ];
        $this->m_user->update_kategori($data_kategori, 'tb_kategori');
        $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Buku berhasil di edit
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>');
        redirect('kategori/index');
    }

    public function delete($id)

    {
        $where = array('id_kategori' => $id);

        $this->m_user->delete($where, 'tb_kategori');
        $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                BUku berhasil dihapus
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>');
        redirect('kategori/index');
    }

    public function search()
    {
        $keyword = $this->input->post('keyword');
        $data['tb_kategori'] = $this->m_user->get_keywordKategori($keyword);
        $this->load->view('latihan/table_kategori', $data);
    }
}
