<?php

class Dashboard extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_user');
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));
            if (!$this->session->userdata('email')) {
                redirect('auth');
            }
    }

    public function index()
    {

        // $sess = $this->session->set_userdata('login');
        $data['buku'] = $this->m_user->jumlah_buku();
        $data['kategori'] = $this->m_user->jumlah_kategori();
        $data['user'] = $this->m_user->jumlah_user();
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->view('latihan/dashboard', $data);
    }

    public function profile()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->view('latihan/profile', $data);
    }

    public function masterData()
    {
        $data['buku'] = $this->m_user->jumlah_buku();
        $data['kategori'] = $this->m_user->jumlah_kategori();
        $this->load->view('latihan/master_table', $data);
    }
}
